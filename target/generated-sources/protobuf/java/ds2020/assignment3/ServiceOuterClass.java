// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Service.proto

package ds2020.assignment3;

public final class ServiceOuterClass {
  private ServiceOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_HelloRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_HelloRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_HelloResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_HelloResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_MedicationPlanRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_MedicationPlanRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_MedicationPlanResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_MedicationPlanResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_TakeRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_TakeRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_TakeResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_TakeResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_NotTakeRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_NotTakeRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_NotTakeResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_NotTakeResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_TimeRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_TimeRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ds2020_assignment3_TimeResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ds2020_assignment3_TimeResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\rService.proto\022\022ds2020.assignment3\"3\n\014H" +
      "elloRequest\022\021\n\tfirstName\030\001 \001(\t\022\020\n\010lastNa" +
      "me\030\002 \001(\t\"!\n\rHelloResponse\022\020\n\010greeting\030\001 " +
      "\001(\t\"#\n\025MedicationPlanRequest\022\n\n\002id\030\001 \001(\t" +
      "\"O\n\026MedicationPlanResponse\022\014\n\004name\030\001 \001(\t" +
      "\022\016\n\006dosage\030\002 \001(\t\022\027\n\017intake_interval\030\003 \001(" +
      "\t\"\033\n\013TakeRequest\022\014\n\004name\030\001 \001(\t\"\016\n\014TakeRe" +
      "sponse\"\036\n\016NotTakeRequest\022\014\n\004name\030\001 \001(\t\"\021" +
      "\n\017NotTakeResponse\"\r\n\013TimeRequest\".\n\014Time" +
      "Response\022\r\n\005hours\030\001 \001(\t\022\017\n\007minutes\030\002 \001(\t",
      "2\257\003\n\007Service\022L\n\005hello\022 .ds2020.assignmen" +
      "t3.HelloRequest\032!.ds2020.assignment3.Hel" +
      "loResponse\022I\n\004take\022\037.ds2020.assignment3." +
      "TakeRequest\032 .ds2020.assignment3.TakeRes" +
      "ponse\022R\n\007notTake\022\".ds2020.assignment3.No" +
      "tTakeRequest\032#.ds2020.assignment3.NotTak" +
      "eResponse\022I\n\004time\022\037.ds2020.assignment3.T" +
      "imeRequest\032 .ds2020.assignment3.TimeResp" +
      "onse\022l\n\021getMedicationPlan\022).ds2020.assig" +
      "nment3.MedicationPlanRequest\032*.ds2020.as",
      "signment3.MedicationPlanResponse0\001B\002P\001b\006" +
      "proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_ds2020_assignment3_HelloRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_ds2020_assignment3_HelloRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_HelloRequest_descriptor,
        new java.lang.String[] { "FirstName", "LastName", });
    internal_static_ds2020_assignment3_HelloResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_ds2020_assignment3_HelloResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_HelloResponse_descriptor,
        new java.lang.String[] { "Greeting", });
    internal_static_ds2020_assignment3_MedicationPlanRequest_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_ds2020_assignment3_MedicationPlanRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_MedicationPlanRequest_descriptor,
        new java.lang.String[] { "Id", });
    internal_static_ds2020_assignment3_MedicationPlanResponse_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_ds2020_assignment3_MedicationPlanResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_MedicationPlanResponse_descriptor,
        new java.lang.String[] { "Name", "Dosage", "IntakeInterval", });
    internal_static_ds2020_assignment3_TakeRequest_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_ds2020_assignment3_TakeRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_TakeRequest_descriptor,
        new java.lang.String[] { "Name", });
    internal_static_ds2020_assignment3_TakeResponse_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_ds2020_assignment3_TakeResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_TakeResponse_descriptor,
        new java.lang.String[] { });
    internal_static_ds2020_assignment3_NotTakeRequest_descriptor =
      getDescriptor().getMessageTypes().get(6);
    internal_static_ds2020_assignment3_NotTakeRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_NotTakeRequest_descriptor,
        new java.lang.String[] { "Name", });
    internal_static_ds2020_assignment3_NotTakeResponse_descriptor =
      getDescriptor().getMessageTypes().get(7);
    internal_static_ds2020_assignment3_NotTakeResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_NotTakeResponse_descriptor,
        new java.lang.String[] { });
    internal_static_ds2020_assignment3_TimeRequest_descriptor =
      getDescriptor().getMessageTypes().get(8);
    internal_static_ds2020_assignment3_TimeRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_TimeRequest_descriptor,
        new java.lang.String[] { });
    internal_static_ds2020_assignment3_TimeResponse_descriptor =
      getDescriptor().getMessageTypes().get(9);
    internal_static_ds2020_assignment3_TimeResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ds2020_assignment3_TimeResponse_descriptor,
        new java.lang.String[] { "Hours", "Minutes", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
