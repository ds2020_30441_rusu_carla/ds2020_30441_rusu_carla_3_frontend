package ds2020.assignment3;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.4.0)",
    comments = "Source: Service.proto")
public final class ServiceGrpc {

  private ServiceGrpc() {}

  public static final String SERVICE_NAME = "ds2020.assignment3.Service";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ds2020.assignment3.HelloRequest,
      ds2020.assignment3.HelloResponse> METHOD_HELLO =
      io.grpc.MethodDescriptor.<ds2020.assignment3.HelloRequest, ds2020.assignment3.HelloResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ds2020.assignment3.Service", "hello"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.HelloRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.HelloResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ds2020.assignment3.TakeRequest,
      ds2020.assignment3.TakeResponse> METHOD_TAKE =
      io.grpc.MethodDescriptor.<ds2020.assignment3.TakeRequest, ds2020.assignment3.TakeResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ds2020.assignment3.Service", "take"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.TakeRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.TakeResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ds2020.assignment3.NotTakeRequest,
      ds2020.assignment3.NotTakeResponse> METHOD_NOT_TAKE =
      io.grpc.MethodDescriptor.<ds2020.assignment3.NotTakeRequest, ds2020.assignment3.NotTakeResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ds2020.assignment3.Service", "notTake"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.NotTakeRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.NotTakeResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ds2020.assignment3.TimeRequest,
      ds2020.assignment3.TimeResponse> METHOD_TIME =
      io.grpc.MethodDescriptor.<ds2020.assignment3.TimeRequest, ds2020.assignment3.TimeResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ds2020.assignment3.Service", "time"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.TimeRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.TimeResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ds2020.assignment3.MedicationPlanRequest,
      ds2020.assignment3.MedicationPlanResponse> METHOD_GET_MEDICATION_PLAN =
      io.grpc.MethodDescriptor.<ds2020.assignment3.MedicationPlanRequest, ds2020.assignment3.MedicationPlanResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
          .setFullMethodName(generateFullMethodName(
              "ds2020.assignment3.Service", "getMedicationPlan"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.MedicationPlanRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ds2020.assignment3.MedicationPlanResponse.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ServiceStub newStub(io.grpc.Channel channel) {
    return new ServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class ServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void hello(ds2020.assignment3.HelloRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.HelloResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_HELLO, responseObserver);
    }

    /**
     */
    public void take(ds2020.assignment3.TakeRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.TakeResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_TAKE, responseObserver);
    }

    /**
     */
    public void notTake(ds2020.assignment3.NotTakeRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.NotTakeResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_NOT_TAKE, responseObserver);
    }

    /**
     */
    public void time(ds2020.assignment3.TimeRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.TimeResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_TIME, responseObserver);
    }

    /**
     */
    public void getMedicationPlan(ds2020.assignment3.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.MedicationPlanResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_GET_MEDICATION_PLAN, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_HELLO,
            asyncUnaryCall(
              new MethodHandlers<
                ds2020.assignment3.HelloRequest,
                ds2020.assignment3.HelloResponse>(
                  this, METHODID_HELLO)))
          .addMethod(
            METHOD_TAKE,
            asyncUnaryCall(
              new MethodHandlers<
                ds2020.assignment3.TakeRequest,
                ds2020.assignment3.TakeResponse>(
                  this, METHODID_TAKE)))
          .addMethod(
            METHOD_NOT_TAKE,
            asyncUnaryCall(
              new MethodHandlers<
                ds2020.assignment3.NotTakeRequest,
                ds2020.assignment3.NotTakeResponse>(
                  this, METHODID_NOT_TAKE)))
          .addMethod(
            METHOD_TIME,
            asyncUnaryCall(
              new MethodHandlers<
                ds2020.assignment3.TimeRequest,
                ds2020.assignment3.TimeResponse>(
                  this, METHODID_TIME)))
          .addMethod(
            METHOD_GET_MEDICATION_PLAN,
            asyncServerStreamingCall(
              new MethodHandlers<
                ds2020.assignment3.MedicationPlanRequest,
                ds2020.assignment3.MedicationPlanResponse>(
                  this, METHODID_GET_MEDICATION_PLAN)))
          .build();
    }
  }

  /**
   */
  public static final class ServiceStub extends io.grpc.stub.AbstractStub<ServiceStub> {
    private ServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ServiceStub(channel, callOptions);
    }

    /**
     */
    public void hello(ds2020.assignment3.HelloRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.HelloResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_HELLO, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void take(ds2020.assignment3.TakeRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.TakeResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_TAKE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void notTake(ds2020.assignment3.NotTakeRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.NotTakeResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_NOT_TAKE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void time(ds2020.assignment3.TimeRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.TimeResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_TIME, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getMedicationPlan(ds2020.assignment3.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<ds2020.assignment3.MedicationPlanResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(METHOD_GET_MEDICATION_PLAN, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ServiceBlockingStub extends io.grpc.stub.AbstractStub<ServiceBlockingStub> {
    private ServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ds2020.assignment3.HelloResponse hello(ds2020.assignment3.HelloRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_HELLO, getCallOptions(), request);
    }

    /**
     */
    public ds2020.assignment3.TakeResponse take(ds2020.assignment3.TakeRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_TAKE, getCallOptions(), request);
    }

    /**
     */
    public ds2020.assignment3.NotTakeResponse notTake(ds2020.assignment3.NotTakeRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_NOT_TAKE, getCallOptions(), request);
    }

    /**
     */
    public ds2020.assignment3.TimeResponse time(ds2020.assignment3.TimeRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_TIME, getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<ds2020.assignment3.MedicationPlanResponse> getMedicationPlan(
        ds2020.assignment3.MedicationPlanRequest request) {
      return blockingServerStreamingCall(
          getChannel(), METHOD_GET_MEDICATION_PLAN, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ServiceFutureStub extends io.grpc.stub.AbstractStub<ServiceFutureStub> {
    private ServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ds2020.assignment3.HelloResponse> hello(
        ds2020.assignment3.HelloRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_HELLO, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ds2020.assignment3.TakeResponse> take(
        ds2020.assignment3.TakeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_TAKE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ds2020.assignment3.NotTakeResponse> notTake(
        ds2020.assignment3.NotTakeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_NOT_TAKE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ds2020.assignment3.TimeResponse> time(
        ds2020.assignment3.TimeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_TIME, getCallOptions()), request);
    }
  }

  private static final int METHODID_HELLO = 0;
  private static final int METHODID_TAKE = 1;
  private static final int METHODID_NOT_TAKE = 2;
  private static final int METHODID_TIME = 3;
  private static final int METHODID_GET_MEDICATION_PLAN = 4;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_HELLO:
          serviceImpl.hello((ds2020.assignment3.HelloRequest) request,
              (io.grpc.stub.StreamObserver<ds2020.assignment3.HelloResponse>) responseObserver);
          break;
        case METHODID_TAKE:
          serviceImpl.take((ds2020.assignment3.TakeRequest) request,
              (io.grpc.stub.StreamObserver<ds2020.assignment3.TakeResponse>) responseObserver);
          break;
        case METHODID_NOT_TAKE:
          serviceImpl.notTake((ds2020.assignment3.NotTakeRequest) request,
              (io.grpc.stub.StreamObserver<ds2020.assignment3.NotTakeResponse>) responseObserver);
          break;
        case METHODID_TIME:
          serviceImpl.time((ds2020.assignment3.TimeRequest) request,
              (io.grpc.stub.StreamObserver<ds2020.assignment3.TimeResponse>) responseObserver);
          break;
        case METHODID_GET_MEDICATION_PLAN:
          serviceImpl.getMedicationPlan((ds2020.assignment3.MedicationPlanRequest) request,
              (io.grpc.stub.StreamObserver<ds2020.assignment3.MedicationPlanResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class ServiceDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ds2020.assignment3.ServiceOuterClass.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ServiceDescriptorSupplier())
              .addMethod(METHOD_HELLO)
              .addMethod(METHOD_TAKE)
              .addMethod(METHOD_NOT_TAKE)
              .addMethod(METHOD_TIME)
              .addMethod(METHOD_GET_MEDICATION_PLAN)
              .build();
        }
      }
    }
    return result;
  }
}
