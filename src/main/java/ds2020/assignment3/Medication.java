package ds2020.assignment3;

public class Medication {

    String name;
    String dosage;
    int intervalStart;
    int intervalEnd;

    public Medication(String name, String dosage, int intervalStart, int intervalEnd) {
        this.name = name;
        this.dosage = dosage;
        this.intervalStart = intervalStart;
        this.intervalEnd = intervalEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public int getIntervalStart() {
        return intervalStart;
    }

    public void setIntervalStart(int intervalStart) {
        this.intervalStart = intervalStart;
    }

    public int getIntervalEnd() {
        return intervalEnd;
    }

    public void setIntervalEnd(int intervalEnd) {
        this.intervalEnd = intervalEnd;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "name='" + name + '\'' +
                ", dosage='" + dosage + '\'' +
                ", intervalStart=" + intervalStart +
                ", intervalEnd=" + intervalEnd +
                '}';
    }
}
