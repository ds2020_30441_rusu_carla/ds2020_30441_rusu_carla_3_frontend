package ds2020.assignment3;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.*;
import java.util.List;
import java.util.Timer;

public class GrpcClient {

    public static JList<String> list;
    public static JLabel time;
    public static int hours;
    public static int minutes;
    public static DefaultListModel listModel;
    public static JButton fireButton;
    public static List<Medication> medicationList = new ArrayList<>();

    public static void getMedicationPlan() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
                .usePlaintext()
                .build();

        ServiceGrpc.ServiceBlockingStub stub
                = ServiceGrpc.newBlockingStub(channel);

        Iterator<MedicationPlanResponse> planResponse = stub.getMedicationPlan(MedicationPlanRequest.newBuilder()
                .setId("1")
                .build());
        listModel.clear();
        while(planResponse.hasNext()){
            MedicationPlanResponse response = planResponse.next();

            // 6-12 12-18 18-24
            String intervalStart = "";
            String intervalEnd = "";

            int start = 0;
            int end = 0;

            String[] interval = response.getIntakeInterval().split("-");
            if (interval[2].equals("1")) {
                intervalStart = "18:00";
                start = 18;

                intervalEnd = "24:00";
                end = 24;
            }
            if (interval[1].equals("1")) {
                intervalStart = "12:00";
                start = 12;

                if (intervalEnd.equals("")) {
                    intervalEnd = "18:00";
                    end = 18;
                }
            }
            if (interval[0].equals("1")) {
                intervalStart = "06:00";
                start = 6;

                if (intervalEnd.equals("")) {
                    intervalEnd = "12:00";
                    end = 12;
                }
            }

            Medication medication = new Medication(response.getName(), response.getDosage(), start, end);
            medicationList.add(medication);

            listModel.addElement(response.getName() + ", " + response.getDosage() + ", " + intervalStart + "-" + intervalEnd);
            //System.out.println(response.getName() + ", " + response.getDosage() + ", " + response.getIntakeInterval());
        }
        list.setModel(listModel);
        channel.shutdown();
    }

    public static void take(String name) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
                .usePlaintext()
                .build();

        ServiceGrpc.ServiceBlockingStub stub
                = ServiceGrpc.newBlockingStub(channel);

        TakeResponse response = stub.take(TakeRequest.newBuilder()
                .setName(name)
                .build());

        channel.shutdown();
    }

    public static void notTake(String name) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
                .usePlaintext()
                .build();

        ServiceGrpc.ServiceBlockingStub stub
                = ServiceGrpc.newBlockingStub(channel);

        NotTakeResponse response = stub.notTake(NotTakeRequest.newBuilder()
                .setName(name)
                .build());

        channel.shutdown();
    }

    public static void time() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
                .usePlaintext()
                .build();

        ServiceGrpc.ServiceBlockingStub stub
                = ServiceGrpc.newBlockingStub(channel);

        TimeResponse response = stub.time(TimeRequest.newBuilder()
                .build());

        hours=Integer.parseInt(response.getHours());
        minutes=Integer.parseInt(response.getMinutes());
        Medication m=null;
        for (Medication medication : medicationList) {
            //System.out.println(medication);
            if (hours > medication.intervalEnd) {
                m=medication;
            }
        }

        if (m != null) {
            notTake(m.name);
        }
        medicationList.remove(m);

        String mm = "";
        if (response.getMinutes().length() < 2) {
            mm = "0"+response.getMinutes();
        } else {
            mm = response.getMinutes();
        }

        String hh = "";
        if (response.getHours().length() < 2) {
            hh = "0"+response.getHours();
        } else {
            hh = response.getHours();
        }

        time.setText("ServerTime: "+hh+":"+mm);

        channel.shutdown();
    }

    static class FireListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //This method can be called only if
            //there's a valid selection
            //so go ahead and remove whatever's selected.
            int index = list.getSelectedIndex();

            System.out.println("take: "+listModel.getElementAt(index).toString());
            take(listModel.getElementAt(index).toString());
            listModel.remove(index);

            int size = listModel.getSize();

            if (size == 0) { //Nobody's left, disable firing.
                fireButton.setEnabled(false);

            } else { //Select an index.
                if (index == listModel.getSize()) {
                    //removed item in last position
                    index--;
                }

                list.setSelectedIndex(index);
                list.ensureIndexIsVisible(index);
            }
        }
    }

    static class MedicationPlan extends TimerTask {

        @Override
        public void run() {
            medicationList.clear();
            getMedicationPlan();
        }
    }
    static class ServerTime extends TimerTask{

        @Override
        public void run() {
            time();
        }
    }
    public static void main(String[] args) {
        JFrame frame=new JFrame("Medication Plan");
        frame.setSize(300,300);
        frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        list = new JList<>();
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        list.setVisibleRowCount(-1);
        time = new JLabel("ServerTime: 00:00");
        listModel = new DefaultListModel();
        listModel.addElement("");

        //Create the list and put it in a scroll pane.
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.setVisibleRowCount(5);

        fireButton = new JButton("Take");
        fireButton.setActionCommand("take");
        fireButton.addActionListener(new FireListener());

        //Create a panel that uses BoxLayout.
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane,
                BoxLayout.LINE_AXIS));
        buttonPane.add(fireButton);
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(time);
        buttonPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,10));

        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(250, 220));
        p.add(listScroller);
        p.add(buttonPane, BorderLayout.PAGE_END);

        frame.add(p,BorderLayout.NORTH);
        frame.setVisible(true);


        Timer t = new java.util.Timer(true);
        t.schedule(new ServerTime(),0,5000);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 22);
        calendar.set(Calendar.MINUTE, 50);
        calendar.set(Calendar.SECOND, 0);
        Date time = calendar.getTime();

        Timer t1 = new Timer(true);
        t1.schedule(new MedicationPlan(), time);


    }
}
